/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Navigation = __webpack_require__(1);

var _Navigation2 = _interopRequireDefault(_Navigation);

var _About = __webpack_require__(2);

var _About2 = _interopRequireDefault(_About);

var _CoverImage = __webpack_require__(3);

var _CoverImage2 = _interopRequireDefault(_CoverImage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var App = function App() {
  _classCallCheck(this, App);

  new _CoverImage2.default();
  new _Navigation2.default();
  new _About2.default();
};

new App();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Navigation = function () {
  function Navigation() {
    _classCallCheck(this, Navigation);

    this.changePage = this.changePage.bind(this);

    this.currentActive = document.querySelector('.nav li.active');

    this.bindEvents();
  }

  _createClass(Navigation, [{
    key: 'bindEvents',
    value: function bindEvents() {
      var _this = this;

      var navItems = [].concat(_toConsumableArray(document.querySelectorAll('.nav li')));

      navItems.forEach(function (el, i) {
        el.addEventListener('click', function (e) {
          if (!e.target.classList.contains('active')) {
            _this.changePage(e.target);
          }
        });
      });
    }
  }, {
    key: 'changePage',
    value: function changePage(target) {
      var pageToShow = document.getElementById(target.dataset.href);
      var pageToHide = document.querySelector('.page.active');

      this.currentActive.classList.remove('active');

      this.currentActive = target;
      target.classList.add('active');

      if (pageToShow && pageToHide) {
        // Prevent errors if page is not implemented
        pageToHide.classList.add('none');
        pageToHide.classList.remove('active');
        pageToShow.classList.remove('none');
        pageToShow.classList.add('active');
      }
    }
  }]);

  return Navigation;
}();

exports.default = Navigation;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var About = function () {
  function About() {
    _classCallCheck(this, About);

    this.bindEvents();
  }

  _createClass(About, [{
    key: 'getEditboxLabelMessage',
    value: function getEditboxLabelMessage(type) {
      var map = {
        name: 'Name',
        website: 'Website url',
        phone: 'Phone number',
        address: 'City, State & Zip'
      };

      return map[type];
    }
  }, {
    key: 'bindEvents',
    value: function bindEvents() {
      var self = this;

      // bind mobile events
      if (window.innerWidth <= 768) {
        document.querySelector('.js-edit-about').addEventListener('click', function (e) {
          self.createMobileEditBox(this);
        });

        return;
      }

      var actions = [].concat(_toConsumableArray(document.querySelectorAll('.about__item-edit')));

      actions.forEach(function (el, i) {
        el.addEventListener('click', function (e) {
          if (e.target.matches('.about__item-edit') || e.target.matches('.ion-edit')) {
            self.openEditBox(this);
          }
        });
      });
    }
  }, {
    key: 'createMobileEditBox',
    value: function createMobileEditBox(editButton) {
      var page = editButton.parentElement;

      // hide edit button and content
      editButton.style.display = 'none';
      page.querySelector('.page__content').classList.add('none');

      var html = '<div class="editbox__mobile">\n      <div class="flex editbox__mobile-actions">\n        <button class="button__primary editbox__button-cancel">CANCEL</button>\n        <button class="button__secondary editbox__button-save">SAVE</button>\n      </div>\n      <div class="editbox__content">' + this.getInputGroups(page) + '</div>\n    </div>';

      page.insertAdjacentHTML('beforeend', html);
      this.bindEditboxEventsMobile(page, editButton);
    }
  }, {
    key: 'getInputGroups',
    value: function getInputGroups(page) {
      var _this = this;

      var lis = [].concat(_toConsumableArray(page.querySelectorAll('li.flex')));

      return lis.reduce(function (accumulator, el, index) {
        var type = el.dataset.type;

        var val = el.querySelector('.js-text').innerText;
        var label = _this.getEditboxLabelMessage(type);

        return accumulator + ('<div>\n        <label for="' + type + '">' + label + '</label>\n        <input type="text" name="' + type + '" value="' + val + '" id="' + type + '" class="editbox__input">\n      </div>');
      }, '');
    }
  }, {
    key: 'bindEditboxEventsMobile',
    value: function bindEditboxEventsMobile(page, editButton) {
      var editbox = document.querySelector('.editbox__mobile');

      function closeMobileEditBox() {
        page.removeChild(editbox);
        editButton.style.display = 'block';
        page.querySelector('.page__content').classList.remove('none');
      }

      editbox.querySelector('.editbox__button-cancel').addEventListener('click', function (e) {
        closeMobileEditBox();
      });

      editbox.querySelector('.editbox__button-save').addEventListener('click', function (e) {
        var inputs = [].concat(_toConsumableArray(editbox.querySelectorAll('.editbox__input')));

        inputs.forEach(function (el) {
          page.querySelector('[data-type="' + el.id + '"] .js-text').innerText = el.value;
        });

        closeMobileEditBox();
      });
    }
  }, {
    key: 'openEditBox',
    value: function openEditBox(target) {
      var parent = target.parentElement;
      var text = parent.querySelector('.js-text').innerText;
      var label = this.getEditboxLabelMessage(parent.dataset.type);
      var html = '<div class="editbox">\n      <label for="' + parent.dataset.type + '">' + label + '</label>\n      <input type="text" class="editbox__input" id="' + parent.dataset.type + '" value="' + text + '">\n      <div class="flex">\n        <button class="button__secondary editbox__button-save">SAVE</button>\n        <button class="button__primary editbox__button-cancel">CANCEL</button>\n      </div>\n    </div>';

      target.insertAdjacentHTML('beforeend', html);

      this.bindEditboxEvents();
    }
  }, {
    key: 'bindEditboxEvents',
    value: function bindEditboxEvents() {
      var editbox = document.querySelector('.editbox');

      document.querySelector('.editbox__button-cancel').addEventListener('click', function (e) {
        editbox.parentElement.removeChild(editbox);
      });

      document.querySelector('.editbox__button-save').addEventListener('click', function (e) {
        var input = editbox.querySelector('.editbox__input');
        var li = editbox.parentElement.parentElement;

        console.log('save value to database', input.value);

        li.querySelector('.js-text').innerText = input.value;

        editbox.parentElement.removeChild(editbox);
      });
    }
  }]);

  return About;
}();

exports.default = About;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CoverImage = function () {
  function CoverImage() {
    _classCallCheck(this, CoverImage);

    this.bindEvents();
  }

  _createClass(CoverImage, [{
    key: 'bindEvents',
    value: function bindEvents() {
      var fileInput = document.querySelector('.js-file-input');

      document.querySelector('.upload__button').addEventListener('click', function (e) {
        fileInput.click();
      });

      fileInput.addEventListener('change', this.uploadImage, false);
    }
  }, {
    key: 'uploadImage',
    value: function uploadImage(e) {
      var reader = new FileReader();
      reader.onload = function () {
        document.querySelector('.header__top').style.backgroundImage = 'url(' + reader.result + ')';
        console.log('save image to database');
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  }]);

  return CoverImage;
}();

exports.default = CoverImage;

/***/ })
/******/ ]);