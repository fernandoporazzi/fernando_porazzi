const gulp = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const sass = require('gulp-sass');

gulp.task('views', () => {
  return gulp.src('./app/views/**/*.html')
    .pipe(nunjucksRender({
      path: ['app/views/'] // String or Array
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass', () => {
  return gulp.src('./app/sass/**/*.scss')
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .on('error', sass.logError)
    .pipe(gulp.dest('dist'))
});

gulp.task('build', ['views', 'sass']);

gulp.task('dev', ['build'], () => {
  gulp.watch('./app/views/**/*.html', ['views']);
  gulp.watch('./app/sass/**/*.scss', ['sass']);
} );