import Navigation from './Navigation';
import About from './About';
import CoverImage from './CoverImage';

class App {
  constructor() {
    new CoverImage();
    new Navigation();
    new About();
  }
}

new App();