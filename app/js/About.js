export default class About {
  constructor() {
    this.bindEvents();
  }

  getEditboxLabelMessage(type) {
    const map = {
      name: 'Name',
      website: 'Website url',
      phone: 'Phone number',
      address: 'City, State & Zip'
    };

    return map[type];
  }

  bindEvents() {
    let self = this;

    // bind mobile events
    if (window.innerWidth <= 768) {
      document.querySelector('.js-edit-about').addEventListener('click', function(e) {
        self.createMobileEditBox(this);
      });

      return;
    }

    const actions = [...document.querySelectorAll('.about__item-edit')];

    actions.forEach( (el, i) => {
      el.addEventListener('click', function(e) {
        if (e.target.matches('.about__item-edit') || e.target.matches('.ion-edit')) {
          self.openEditBox(this);
        }
      });
    } )
  }

  createMobileEditBox(editButton) {
    const page = editButton.parentElement;

    // hide edit button and content
    editButton.style.display = 'none';
    page.querySelector('.page__content').classList.add('none');

    const html = `<div class="editbox__mobile">
      <div class="flex editbox__mobile-actions">
        <button class="button__primary editbox__button-cancel">CANCEL</button>
        <button class="button__secondary editbox__button-save">SAVE</button>
      </div>
      <div class="editbox__content">${this.getInputGroups(page)}</div>
    </div>`;

    page.insertAdjacentHTML('beforeend', html);
    this.bindEditboxEventsMobile(page, editButton);
  }

  getInputGroups(page) {
    const lis = [...page.querySelectorAll('li.flex')];

    return lis.reduce( (accumulator, el, index) => {
      const {type} = el.dataset;
      const val = el.querySelector('.js-text').innerText;
      const label = this.getEditboxLabelMessage(type);

      return accumulator + `<div>
        <label for="${type}">${label}</label>
        <input type="text" name="${type}" value="${val}" id="${type}" class="editbox__input">
      </div>`;
    }, '' );
  }

  bindEditboxEventsMobile(page, editButton) {
    const editbox = document.querySelector('.editbox__mobile');

    function closeMobileEditBox() {
      page.removeChild(editbox);
      editButton.style.display = 'block';
      page.querySelector('.page__content').classList.remove('none');
    }

    editbox.querySelector('.editbox__button-cancel').addEventListener('click', (e) => {
      closeMobileEditBox();
    })

    editbox.querySelector('.editbox__button-save').addEventListener('click', (e) => {
      const inputs = [...editbox.querySelectorAll('.editbox__input')];

      inputs.forEach(el => {
        page.querySelector(`[data-type="${el.id}"] .js-text`).innerText = el.value;
      });

      closeMobileEditBox();
    });
  }

  openEditBox(target) {
    const parent = target.parentElement;
    const text = parent.querySelector('.js-text').innerText;
    const label = this.getEditboxLabelMessage(parent.dataset.type);
    const html = `<div class="editbox">
      <label for="${parent.dataset.type}">${label}</label>
      <input type="text" class="editbox__input" id="${parent.dataset.type}" value="${text}">
      <div class="flex">
        <button class="button__secondary editbox__button-save">SAVE</button>
        <button class="button__primary editbox__button-cancel">CANCEL</button>
      </div>
    </div>`;

    target.insertAdjacentHTML('beforeend', html);

    this.bindEditboxEvents();
  }

  bindEditboxEvents() {
    const editbox = document.querySelector('.editbox');

    document.querySelector('.editbox__button-cancel').addEventListener('click', (e) => {
      editbox.parentElement.removeChild(editbox);
    });

    document.querySelector('.editbox__button-save').addEventListener('click', (e) => {
      const input = editbox.querySelector('.editbox__input');
      const li = editbox.parentElement.parentElement;

      console.log('save value to database', input.value);

      li.querySelector('.js-text').innerText = input.value;

      editbox.parentElement.removeChild(editbox);
    });
  }

}