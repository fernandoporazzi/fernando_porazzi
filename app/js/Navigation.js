export default class Navigation {
  constructor() {
    this.changePage = this.changePage.bind(this);

    this.currentActive = document.querySelector('.nav li.active');

    this.bindEvents();
  }

  bindEvents() {
    const navItems = [...document.querySelectorAll('.nav li')];

    navItems.forEach( (el, i) => {
      el.addEventListener('click', (e) => {
        if (!e.target.classList.contains('active')) {
          this.changePage(e.target);
        }
      });
    } );
  }

  changePage(target) {
    const pageToShow = document.getElementById(target.dataset.href);
    const pageToHide = document.querySelector('.page.active');

    this.currentActive.classList.remove('active');

    this.currentActive = target;
    target.classList.add('active');

    if (pageToShow && pageToHide) { // Prevent errors if page is not implemented
      pageToHide.classList.add('none');
      pageToHide.classList.remove('active');
      pageToShow.classList.remove('none');
      pageToShow.classList.add('active');
    }
  }
}