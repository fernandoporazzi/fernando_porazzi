export default class CoverImage {
  constructor() {
    this.bindEvents();
  }

  bindEvents() {
    const fileInput = document.querySelector('.js-file-input');

    document.querySelector('.upload__button').addEventListener('click', (e) => {
      fileInput.click();
    });

    fileInput.addEventListener('change', this.uploadImage, false);
  }

  uploadImage(e) {
    const reader = new FileReader();
    reader.onload = () => {
      document.querySelector('.header__top').style.backgroundImage = `url(${reader.result})`;
      console.log('save image to database');
    }
    reader.readAsDataURL(e.target.files[0]);
  }
}