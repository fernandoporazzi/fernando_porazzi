const path = require('path');

module.exports = {
  entry: './app/js/index.js',

  output: {
    filename: `bundle.js`,
    path: path.resolve(__dirname, 'dist/')
  },

  module: {
    rules: [
      {
        test: /\.js?$/,
        include: [
          path.resolve(__dirname, './app/js')
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules')
        ],
        loader: 'babel-loader',
        options: {
          presets: ['es2015']
        }
      }
    ]
  },

  resolve: {
    modules: [
      'node_modules'
    ]
  },
};